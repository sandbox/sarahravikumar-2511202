=====
Block Background
http://drupal.org/project/block_background

RECOMMENDED MODULES
-------------------
 * Blocks
   
INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------
1. Enable the module
2. To add a bakkground image to a block, simply visit that block's configuration page at
Administration > Structure > Blocks
